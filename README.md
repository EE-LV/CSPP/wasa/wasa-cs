WASA-CS README
====================
This LabVIEW project _WASA-CS.lvproj_ is used to develop a control and monitoring system for the WASA detector based on NI ActorFramework and CS++ libraries.

Currently used development SW is LabVIEW 2019.

Related documents and information
=================================
- README.md
- Release_Notes.md
- EUPL v.1.1 - Lizenz.pdf & EUPL v.1.1 - Lizenz.rtf
- Contact: your email
- Download, bug reports... : Git Repository URL
- Documentation:
  - Refer to Documantation folder 
  - NI Actor Framework: https://ni.com/actorframework
  - CS++
	- https://git.gsi.de/EE-LV/CSPP/CSPP/wikis/home
    - https://git.gsi.de/EE-LV/CSPP/CSPP_Documentation
  
Included Submodules
===================
- [Packages/CSPP_Core](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): This package is used as submodule.
- [Packages/CSPP_ObjectManager](https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager): This package is used as submodule.
- [Packages/CSPP_DSC](https://git.gsi.de/EE-LV/CSPP/CSPP_DSC): Containing DSC Alarm- & Trend-Viewer
- [Packages/CSPP_Utilities](https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities): Providing some usefull utility classes. 

Optional Submodules
-------------------
- [Packages/CSPP_DeviceBase](https://git.gsi.de/EE-LV/CSPP/CSPP_DeviceBase): Definition of CS++Device ancestor classes
- [Packages/CSPP_IVI](https://git.gsi.de/EE-LV/CSPP/CSPP_IVI): Implementations of derived CS++Device classes using IVI driver
- [Packages/CSPP_LNA](https://git.gsi.de/EE-LV/CSPP/CSPP_LNA): Extends the Linked Network Actor to support zero coupled messages.
- [Packages/CSPP_RT](https://git.gsi.de/EE-LV/CSPP/CSPP_RT): Providing a librarie supporting LabVIEW-RT features. 
- [Packages/CSPP_PVConverter](https://git.gsi.de/EE-LV/CSPP/CSPP_PVConverter): Providing support for e.g. log-scaling of PV or conversion to array. 
- [Packages/CSPP_Syslog](https://git.gsi.de/EE-LV/CSPP/CSPP_Syslog): Providing a Syslog based Message Handler 

Refer to https://git.gsi.de/EE-LV/CSPP for more available CS++ submodules.

Optional External Dependencies
=================================
- Monitored Actor; Refer to
  - https://decibel.ni.com/content/thread/18301 and
  - http://lavag.org/topic/17056-monitoring-actors

Getting started:
=================================
- Install __CSPP_Tools__
  - Clone [CSPP_Tools](https://git.gsi.de/EE-LV/CSPP/CSPP_Tools)
  - Get submodules:
    - `git submodule init`
    - `git submodule update`
    - _Optionally switch to the most recent branch._
  - Mass-compile folder `CSPP_Tools`
  - Run `CSPP_Tools\Main-Project\Installer.vi`
- Fork __this__ repository, if not alread done, and rename repository name and path. Refer to repository settings.
- Clone the forked repository to a local folder.
- Switch to the desired branch.
- Get submodules:
  - `git submodule init`
  - `git submodule update`
  - _Optionally switch to the most recent branch._
- `chmod a-w -R *` to avoid unintended changes.
- Optionally create a hard link to the custom error file(s): 
  - cd <LabVIEW>\user.lib\errors
  - mklink /h CSPP_Core-errors.txt Packages\CSPP_Core\CSPP_Core-errors.txt
- Copy `WASA-CS.lvproj` to `YourProject.lvproj`
- Open `YourProject.lvproj` with LabVIEW
- Copy following files:
  - `WASA-CS.ini` to `YourProject.ini`
  - `WASA-CS_Main.vi` to `YourProject_Main.vi`
  - `WASA-CS_Content.vi` to `YourProject_Content.vi`
- You need to create and deploy your project specific shared variable libraries.
  - Sample shared variable libraries should be available on disk in the corresponding package folder.
- Run your project specific `YourProject_Main.vi` in order to check if everything is working.
- Extend `YourProject.lvproj` to your needs.

Known issues:
=============

Author: H.Brand@gsi.de

Copyright 2019  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.