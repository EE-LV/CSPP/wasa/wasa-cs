Release Notes for the WASA-CS Project
===========================================
This LabVIEW project _WASA-CS.lvproj_ is used to develop a control and monitoring system for the WASA detector based on NI ActorFramework and CS++ libraries.

Version 0.0.0.0 23-07-2019 H.Brand@gsi.de
--------------------------------------
The WASA-CS project was just forked. There is the master branch with some submodules, only.

